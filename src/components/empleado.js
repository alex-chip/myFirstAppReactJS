import React, { Component } from 'react'

class Empleado extends Component{
  render(){
    return(
      <li>
        <img src="{this.props.imagen}"  role="presentation"/>
        {this.props.nombre} - {this.props.email}
      </li>
    )
  }
}

export default Empleado